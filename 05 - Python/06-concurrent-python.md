# Python 3
## Concurrent python

### Objective
Context: in python 2 the way to go because the issue with the Global Interpreter Lock (GIL)
was to use only processes (forking your program) and try to avoid threads. However in Python 3
the "asyncio" library provides a way using async/await to have a concurrent python. 

### Instructions

- Take a look to https://realpython.com/async-io-python/
- Try to run the basic example

### Expected deliverables
- NA - just catch the concepts for concurrency, parallelism and a think in a use
  case where this could fit. (When python alone is not longer enough)

### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Concurrent | A description of what is concurrency and that concurrency is not necessary parallelism | 100 %

