# Python 3
## Classes, decorators and lambdas

### Objective
To learn about how to create a class in python and how to use them across a functional module or code.
To learn anonymous function (lambda). Lambdas are handy for short-usage for generators, or filters.
Most of us don't create libraries, we use them. But the decorator in python provides an easy way
to end-users to get and apply external libraries i.e Flask, Qt and other frameworks are based on 
decorators usage

### Instructions

- Find a pattern where lambdas might be useful 
- Create a class decorator and give it to your peers, and let them find out how to use it. 

### Expected deliverables
- Simple python script that uses a library that works using decorators and lambdas. 

### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Lambda | A description of what is for and what is not | 50% |
| Decorators  | Try to settle down the idea how to use them | 50% |

