# Ansible

## Goals
This course focuses on the main basics for one of the most  popular tools for server management, Ansible. Trainee will be able to understand how Ansible works based in orchestration & idempotency using handly lenguage based in YAML files, called **"playbooks"**.

## Objectives
- Identify the basic workflow for Ansible.
- Provide the trainee with important concepts from server management: **orchestrate** and **idempotency**
- Define rules to write functional and easy-readable "plays" along with "playbooks".
- Analyze when is a proper scenary to develop "playbooks" and when isn't.

## Contents
- [Basic concepts for Ansible.](./content/Basic-concepts.md)
- [Defining basic concepts used by Ansible (Orchestration and Idempotency).](./content/Orchestration-&-Idempotency.md)
- [Syntax used with "Plays" and "Playbooks".](./content/Syntax-play.md)
- [Installation under GNU/Linux and Mac OS.](./content/Installation.md)
- [Theory implied under Playbooks/Inventory.](./content/Inventory-Theory.md)

## Learning materials
- [Ansible Best practices.](./material/Ansible-Best-Practices.pdf)
- [Ansible presentation course](./material/Ansible-presentation.pdf)
- [Using Variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)
- [Using Roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
- [Templates](https://docs.ansible.com/ansible/latest/modules/template_module.html)
- [Modules (Linux Only)](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html)


## Course activities
- [Working with loops](./activities/01-working-with-loops.md)
- [Working with Adhoc commands](./activities/02-adhoc.md)
- [Provisioning your nodes!](./activities/03-provisioning.md)
- [All in one](./activities/04-All-in-one.md)
- [Using templates](./activities/05-Templates.md)


## Collaborators
| Name | Slack | EMAIL |
| ---- | ----- | ----: |
| Miguel Angel H. | @Miguel Hernandez | miguel.hernandez@digitalonus.com |