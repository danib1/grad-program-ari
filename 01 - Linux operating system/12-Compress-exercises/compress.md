## File compression

Present an example on how to extract/compress with the following formats:

### Tar / tar.gz / tar.bz2
> yum install tar

> tar -cvfz $dst $src

> tar -cvfz myfile.tar.gz myfile.img


Files that have a .tar.gz or a .tar.bz2 extension are compressed archive files. A file with just a .tar extension is uncompressed.

The .tar portion of the file extension stands for tape archive, back in 1979, the tar command was created to allow system administrators to archive files onto tape.

The .gz or .bz2 extension suffix indicates that the archive has been compressed, using either the gzip or bzip2 compression algorithm. The tar command will work with both types of file.

### Gzip

> yum install gzip

> gzip myfile

This will output myfile.gz compressed.

> gzip -d myfile.gz

This is to uncompress.

### Bzip2

compress
> bzip2 myfile

decompress
> bzip2 -d myfile.bz2

You can also use the -v for verbose.

### References

[top 15 utilities](https://www.unixmen.com/top-15-file-compression-utilities-linux/)

[How to extract files](https://www.howtogeek.com/409742/how-to-extract-files-from-a-.tar.gz-or-.tar.bz2-file-on-linux/)
