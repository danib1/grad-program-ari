# Learning Vi and Vim

Vim was first created as an imitation of Vi, but it is now known as Vi improved.
It was created in 1988 but not released until 1991.

Vim is the default command line editor for all linux systems.

It is important to learn it because no matter what distribution you may end up using, vim will always be an existing tool.

Vim is an industry standard, even though there exist modern editors it's always a handy skill to know how to edit in Vim.

It's lightweight and trustworthy.

It's better to adapt to the default tools than to try to adapt the system to you. We're in an industry where continuously learning is a way of survival, so if you're not familiar yet with vim, take a dive and start the adventure!

## Steps to create a file with vim

1. Open a terminal

2. Type

    vim <name of your file>`

## Vim modes

* __normal__

For navigation and manipulation of text.

This is the default mode.

* __insert__

For editing.

* __command line__

To enable/disable features. To configure the editor. Or other advanced features.

## How to edit a file

Use the following keys to:

* __i__ : start typing
* __x__ : delete characters
* __dd__ : delete the whole line
* __wd__ : delete a word
* __/__ : to search for a regular expression

## Commands

The commands that I find the most useful are:

* __:x__ : To save and quit
* __:q!__ : To exit without saving
* __:q__ : quit when changes have not been made
* __:set number__ : to show line numbers
* __:ignore case__ : to unset case sensitive search
* __y__ : yank or cut
* __p__ : put or paste
* __:[line number]__ : go to line numbers
* __u__ : undo
* __:red__ : redo


## Some tips

You can use the command view instead of vi if you don't plan to edit the document at all.

If you are in a not so basic pc you can use Gvim and use the commands and extensibility of vim with some GUI features.

You can change the theme of Vim (colors), explore the possibilites [here](https://vimcolors.com/)

You can launch the tutorial at anytime by typing `:Tutor`
