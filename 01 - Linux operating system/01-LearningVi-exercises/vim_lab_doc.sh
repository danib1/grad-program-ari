# create the files
for i in {1..5}
do
  touch file0$i
done

vim file01
#enter insert mode
i
# write The message
Welcome to DigitalOnUs
# go to normal mode and copy and paste n times
[Esc]
yy28332p
# it is one less than desired because we already have one line.
# use the line numbers command to verify that the number of lines is correct
:set number



# The same goes for all the following files but to replace the
# specific line contents, use the command
:[line number]
dd
i
<write the new line content>

# for file05, to search and replace use the following command

:%s/search/replace/g
:%s/company/DigitalOnUs/g
