# About SSH

## Brief History

Secure Shell was created in 1995 by Tatu Ylonen, in Finland.

The focus was to make it secure and encrypted, since other plain text communication tools were already popular. Such as Telnet, rlogin, rsh and rcp.

## Why you should learn SSH

1. __Simplicity__ - easy to use in all kinds of environments. (basic and advanced)
2. __Speed__ - in low-latency LAN and Internet connections.
3. __Security__ - it has more than one authentication option, and with some more configuration it becomes even more robust.
4. __Standard__  - it is the industry standard for remote access protocols.
5. __Multi-purpose__ - it is not only used for remote access to another computer, but also to authenticate your identity to multiple services, an example is git hosts.
---
## How to Configure an ssh server

Go to the file

> /etc/ssh/sshd_config

Change (and uncomment) the following lines

    Port 2222
    Banner /etc/issue.net
    AllowGroups ssh-groups
    PermitRootLogin no
    UseDNS no

### Good practice when editing servers config

_Do not delete the original lines. Leave them commented_

### Configure the Firewall

Open the door only on this login session. Forever.

> firewall-cmd --add-port=2222/tcp --permanent

### Restart ssh daemon

To restart the ssh daemon you need to have the `semanage` package installed.

> yum install policycoreutils-python

Now, run the following command:

> semanage port -a -t ssh_port_t -p tcp

And now, restart the ssh daemon for the configuration changes to take place.

> systemctl restart sshd

Verify with this.

> systemctl status sshd

### Add a group to linux.

The group we edited into the ssh config file.
> groupadd ssh-groups

Add the normal user to this group (you)
> usermod -a -G ssh-groups $USER

Now, the ssh can only be used with the port 2222. Root doesn't have access. And the firewall is configured. This is an example of a configuration, your needs may be different, but this is an option to make it more secure.

### Modify the Banner

As it is now, there is already a banner file pointed to, but it doesn't have a custom message. Go change it!

Edit the FILE
> /etc/issue.net

![](ref_images/ssh_login.png)

---

## Copy a file to a coworker's host.

Create a file called config inside .ssh/ and write the following contents:

    Host victor
      HostName 192.168.99.246
      User vhugo
      Port 2222

Change the permissions of the file
> chmod 600 .ssh/config

Now you can login with a shortname to such host. If you know their password.

![](ref_images/buddy.png)

## SSH keys

How to generate a ssh-key

>ssh-keygen

It will be located here by default.
>.ssh/id_rsa

To send your key to another server

> ssh-copy-id $hostname

> ssh-copy-id victor


### References

[SSH and remote access](https://www.jeffgeerling.com/blog/brief-history-ssh-and-remote-access)
