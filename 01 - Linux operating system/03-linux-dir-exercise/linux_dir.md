# Linux Directories

Below is a table of the Linux default directories in alphabetical order.

Everything the operating system needs to work is here:

| Dir | Brief description |
| -- | -- :|
| / | root |
| /bin | store user commands |
| /boot | contains files used for system startup including the kernel |
| /dev | device files |
| /etc | configuration files |
| /home | user's home directories |
| /initrd | is used to load required device modules and mount the initrd.img image file during system startup |
| /lib | library files used by programs that are in /bin |
| /lost+found | files without names found by fsck |
| /mnt | mount points for file systems that were mounted after boot |
| /opt | used primarily for installation of third-party software (seen as optional software) |
| /proc | holds system information required by certain programs |
| /root | home directory of the user root |
| /sbin | system commands |
| /tmp | temporary. All users have read and write access |
| /usr | contains files related to users |
| /var | holds files that are constantly changing |


## References

[Search it channel, Linux Directories](https://searchitchannel.techtarget.com/feature/Linux-UNIX-directories-and-shell-commands-for-VARs)
