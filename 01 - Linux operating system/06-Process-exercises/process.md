# Processes

A process refers to a program in execution;
it’s a running instance of a program.
It is made up of the program instruction,
data read from files, other programs or input from a system user.

__There are fundamentally two types of processes in Linux:__

- _Foreground processes_ (also referred to as interactive processes) – these are initialized and controlled through a terminal session. In other words, there has to be a user connected to the system to start such processes; they haven’t started automatically as part of the system functions/services.

- _Background processes_ (also referred to as non-interactive/automatic processes) – are processes not connected to a terminal; they don’t expect any user input.

---

* Daemons:
These are special types of background processes that start at system startup and keep running forever.

---

### How the OS identifies processes.

* Parent processes – they can create more processes.

* Child processes – these processes are created by other processes during run-time.

---

### State of the processes.

In Linux, a process has the following possible states:

* Running – here it’s either running (it is the current process in the system) or it’s ready to run (it’s waiting to be assigned to one of the CPUs).

* Waiting – in this state, a process is waiting for an event to occur or for a system resource. Additionally, the kernel also differentiates between two types of waiting processes;

 * interruptible waiting processes – can be interrupted by signals

 * uninterruptible waiting processes – are waiting directly on hardware conditions and cannot be interrupted by any event/signal.

* Stopped – in this state, a process has been stopped, usually by receiving a signal. For instance, a process that is being debugged.

* Zombie – here, a process is dead, it has been halted but it’s still has an entry in the process table.

---

### How can you list the different kind of processes? (4 different utilities)

> pstree

> ps -ax

> htop

> glances

### How do you kill a process gracefully?

> kill -15 $pid

> kill -SIGTERM $pid

If you don't make the signal explicit, it doesn't matter as it is the default option. It is the safest way.

### How do you kill a process in the least graceful way?

Use SIGKILL as a last resort to kill process. It will not save data or cleaning kill the process.

> kill -9 $pid

> kill -SIGKILL $pid

### How to list all the daemons running

When listing processes, look for the ones whose parent pid is 1.

or

> chkconfig --list | grep $(runlevel | awk '{ print $2}'):on

### How can you check if a daemon is enabled?

Replace the service-name for the daemon that you want to find out if it's running.

> systemctl status service-name

### File links

* Soft link: is a link to the original file. It depends on the original file to exist.

>  ln -s source.file softlink.file

* Hard link: is a mirror copy of the original file. It doesn't need the original file anymore.

> ln source.file hardlink.file

### What is an inode?

An inode is a record in a disk table, which contains information about a file or folder. The number of inodes on your account equals the number of files and folders you have on it.

## References

[Linux Process Management](https://www.tecmint.com/linux-process-management/)

[killing processes](https://www.cyberciti.biz/faq/kill-process-in-linux-or-terminate-a-process-in-unix-or-linux-systems/)

[Enabled services](https://www.cyberciti.biz/faq/linux-determine-which-services-are-enabled-at-boot/)

[File links] (https://www.ostechnix.com/explaining-soft-link-and-hard-link-in-linux-with-examples/)

[inodes](https://www.siteground.com/kb/what_is_an_inode/)
