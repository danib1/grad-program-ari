# RPM

### What is an RPM package?

A Redhad Package Manager are used in Redhat, SUSE and its derivatives.

They are program executable files.

### What is a dependency in Linux?
Some tools or programs that are needed in order to run another program.

### What is a repository in Linux?
It's like an app store, just without the Graphical user interface.

### Install an RPM package, document the process


( -i  for install)
> rpm -ivh $archivo_rpm

### Remove an RPM package, document the process
-e for erase
> rpm -e $program

### How can you list all the installed packages using a RPM?
> rpm -ql $name  
