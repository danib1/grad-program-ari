# YUM

## The story

Yum was created as a replacement of YUP, the package manager of YellowDog Linux.

Its documentation was first released in 2003.

By 2005 it was already widely adopted, used by half ot the linux Market.

A very good feature of yum, is that it will install the dependencies of whatever you asked to install automatically (with your permission)

## Config files

Yum uses the following config files.
> /etc/yum.repos.d/


## What is the difference between yum and yum group

In a way, yum group is still part of yum, but with a more specific goal.

A package group is a collection of packages that serve a common purpose.

Installing a package group pulls a set of dependent packages, saving time considerably. The yum groups command is a top-level command that covers all the operations that act on package groups in yum.

it will be used just like

> yum install $NAME

> yum group install $group_name

## How can you list all the installed packages using yum?

> yum list installed

### References

[yum history](https://en.wikipedia.org/wiki/Yum_(software))

[installed packages](https://www.tecmint.com/list-installed-packages-in-rhel-centos-fedora/)
