# Permissions and owners

I learned that each file has in its metadata information about who can access it in different ways.

You can visualize this summary when executing ´list all´ or ´ll´.

The types of access are:
- Read
- Write
- Execute (x)

And such access can be done by
- user owner
- group owner
- others (everyone else)

Resulting in the following matrix, represented with 9 bits. Read in octal with 3 numbers.

| users | groups | others |
| ----------- | ------------- | -------: |
| rwx | rwx | rwx |
| 7 | 7 | 7 |


A file whose permissions read 777 has everything _turned on_

### How to change permissions

> chmod \<number code> \<file>

"_change mode_"

Example

> chmod 412 hola.txt

Would result in:

| users | groups | others |
| ----------- |-------------| -------:|
| rwx | rwx | rwx |
| 4 | 1 | 2 |
| 100 | 001 | 010|
| r-- | --x | -w- |

A longer way, but more verbose is: (_explicit_)
> chmod u=r--,g=--x.o=-w- $file

## Exercise

Create the following directories.

1. _January_ with r-x,-wx,r--.

Owner user is carlos.

Owner group is sofia.

2. _February_ rwx, r--, --x

Owner user is martin

Owner group is dou

3. _March_ r--,--x,-w-

Owner user is juan

Owner group is empresa

4. _April_ -w-,--x,r--

Owner user is ti

Owner group is corp

5. _May_ rw-, r-x, --x

Owner user is rh

Owner group is usuarios

> chown $user $file

> chgrp $group $file

Result image

![](owners.png)

### Create a new user and change the owner of the previous files to this user

![](owners2.png)
