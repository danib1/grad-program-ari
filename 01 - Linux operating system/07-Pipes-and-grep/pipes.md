# Pipes and grep

### What is a pipe and how does it work?

It's a symbol that can be used to control streams of standard output in the linux terminal. It redirects the contents of a command towards another one.

### Explain how grep works and present an example

The grep command uses a regular expression search engine over the next argument with option modifications.

This command will search for the word dani in the current directory, recursively.
> grep -nr "dani" .

### Explain how egrep works and present an example

using egrep is the same as grep -E.
This will help the special characters of regular expressions come into action without escaping them with the backslash.

> egrep -C 0 '(f|g)ile' check_file


### Present an example where you combine a pipe with grep and other command

You may be looking for the contents of a file where it contains the word usb, this is a very simple use of grep.

> cat myfile.txt | grep "usb"

### Present an example where you combine a pipe with egrep and other command

Looking for the occurrence of to letters _a_.

> cat anotherfile.txt | egrep “a{2}”


## References

[egrep examples] (https://www.tecmint.com/difference-between-grep-egrep-and-fgrep-in-linux/)
