# Linux basic commands

Below is a table with 15+ linux basic commands.
A short explanation and an example.

| Command | What is it | Example |
| ------- | ---------- | ------ :|
| ls | list files and directories  | ls -lrta  |
| cp | copy a file | cp file new_name_of_file |
| mv | move or rename a file | mv old_file_path new_file_path|
| rm | delete a file | rm -rf directory_path |
| du | disk usage, tells you the space a file is using | du -h path_to_file  |
| pwd | print working directory, in which path you are located | pwd |
| man | manual of a command | man cp |
| mkdir | creates a new directory | mkdir my_project |
| cat | display to stdout the contents of a file | cat hello.txt |
| more/less | very simple text editor | more text_file|
| cd | change directory | cd .. |
| touch | make a new empty file (0 bytes) | touch newfile.txt |
| clear | clear the console text | clear |
| tail | output the last 10 lines of a file | tail my_log.log |
| head | output the first 10 lines of a file | head config_file.s |
| history | will show your previous commands used in a session | history |
| eject | ejects a physical disk | eject |
