# System ctl

## SystemD
SystemD has come to replace systemV.

SystemD is a software suite that provides an array of components for Linux operating systems.

It started being developed in 2010. In 2011 Fedora enabled it by default. And in 2014, Ubuntu shipped with it.

All the commands that start with systemctl are part of systemd.

## What's the difference between enabled and running?

Systemd provides a dependency system between various entities called "units".

Units encapsulate various objects that are relevant for system boot-up and maintenance.

Such units can have several states:

* enabled - a service (unit) is configured to start when the system boots
* disabled - a service (unit) is configured to not start when the system boots
* active - a service (unit) is currently running.
* inactive - a service (unit) is currently not running, but may get started, i.e. become active, if something attempts to make use of the service.

Therefore, it it's enabled, it's not necesarilly running, it just was instructed to start on boot, but it may have been stopped later.

## How to enable and start a daemon

> systemctl enable application.service

> systemctl start application.service


## What are the available targets in centos 7? (multi-user, desktop, etc)

In Centos and RHEL (red hat) target units allow you to start a system with only the services that are required for a specific purpose.

A set of predefined targets is distributed as follows:

| Target Unit| Description |
| --| --|
| poweroff | shut down and power off|
| rescue | set up a rescue shell |
| nongraphical multi-user | set up a nongraphical multi-user shell |
| graphical multi-user | set up a graphical multi-user shell|
| reboot| shut down and reboot the system|



### References

[Digital ocean tutorial](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)

[Systemd targets](https://www.thegeekdiary.com/centos-rhel-7-begginners-guide-to-systemd-targets-replacement-of-sysv-init-run-levels/)
