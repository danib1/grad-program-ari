# Users and groups

### How to create a user in Linux


simple way:
> useradd $user

with more metadata:
> useradd -c "name lastname - mail@server.com" -G wheel,ssh-groups juan

### How to delete a user in Linux

> userdel -r juan

### How to modify a user in Linux

>usermod [options] $USER

To learn more about the options, [click here](https://www.tecmint.com/usermod-command-examples/)

### How to change user password

>passwd

This command will prompt you the current password and then will let you enter a new one for your own user.

> passwd $user

This is to change the password for a different user than the one you're currently logged in with.

### How to list users in Linux

This will show you the connected users.
> who

![](ref_images/who.png)


This will show you information about all the users. The first column is the username. And the second column is the encrypted password.
> less /etc/passwd

![](ref_images/passwd.png)

The w command provides a quick summary of every user logged into a computer.
>w

![](ref_images/w.png)

The finger command, has many options, you can learn about them [here](https://www.computerhope.com/unix/ufinger.htm)

The finger command also displays users logged into the system.
> finger [options] $user $user@host

![](ref_images/finger.png)

The [id](https://www.geeksforgeeks.org/id-command-in-linux-with-examples/) command is used to find out user and group names and numerid ID's

When used with no options, it will display info about the user you're logged in with.
> id [option] [USER]

![](ref_images/id1.png)

![](ref_images/id2.png)

## Exercise
Create a group in Linux called dou-devops
> groupadd dou-devops

Add the users dou-YOUR_NAME to the previous group
>useradd -G dou-devops dou-dani

![](ref_images/dou-dani.png)

Delete the user dou-YOUR_NAME users from the dou-devops group

> usermod -G "" dou-dani

![](ref_images/dou-dani2.png)


### How to consult groups in Linux

>groups

With the groups command. Learn more [here](https://linuxize.com/post/how-to-list-groups-in-linux/)

### How to delete groups in Linux?

With the user mod command, it works as well for adding groups as for ommitting them.

### Different methods to use root privileges

> sudo su -

You will use your user's password, and still turn into root.

> su -

Act as root from another username

__DO NOT__ omit the dash, if you do, the environment variables of the user are brought to the root workspace.


Using the dash is the best practice.

You will need the root password.

### References
[User passwords](https://www.cyberciti.biz/faq/linux-set-change-password-how-to/)
