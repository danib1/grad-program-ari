# Linux Operating System
# Learning some basic commands in Linux

### Instructions
Explain and give an example of the following commands:
```
ls, cp, mv, rm, du, df, pwd, mkdir, cat, more, less, cd, touch, clear, tail, head, history, eject, uname, blkid, lspci, file, date, clock, uptime, last, man, last login, poweroff, reboot, exit
```

### Expected deliverables
- MD file with each activity question answered
- Documentation of procedures, include images if needed



### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Commands   | At least 15 of them were explained with an example | %100 |
