# Git
## Git stash

### Instructions
Using your fork from activity [02-git-collaboration.md](./02-git-collaboration.md) prepare a new feature and mock up a solution, you'll have to stash your changes for unforeseen reasons.

After a while you'll be able to continue to work on this other feature, restore the changes that you stashed and document the whole process.

In your own words, tell us the proper scenarios of when to stash uncommitted changes and when is not a good idea.


### Expected deliverables
- MD file with each activity question answered
- Documentation of procedures, include images if needed



### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Stash process   | All the necessary steps were documented | 70% |
| Stash usage   | Was able to discriminate proper usage of `git stash` | 30% |
