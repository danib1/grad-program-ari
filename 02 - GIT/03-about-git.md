# Git
## About git

### Instructions
- Besides being useful as a SCM, what influence does git has in the industry?
- What options do you have to initialize a repo? (cli and gui)
- Explain a strategy to avoid git conflicts
- Tell us why are branches important
- Explain the difference between the working directory and staging area



### Expected deliverables
- MD file with each activity question answered
- Documentation of procedures, include images if needed



### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Git influence   | Git influence was explained in his/her own words | 20% |
| Initialize repo   | Two options were detailed | 20% |
| Conflicts   | At least one strategy to avoid git conflicts was explained| 20% |
| Branches   | Branches importance was explained in his/her own words | 20% |
| Working directory/staging   | Was able to explain in his/her own words the difference between the working directory and staging area | 20% |
