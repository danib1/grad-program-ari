# Git
## Git conflicts

### Instructions
Work in pairs and using your what you learned so far, provoke a git conflict and document the process to resolve it, use the tool of your choice.


### Expected deliverables
- Documentation of procedures, include images if needed



### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Git conflict   | Document the process to resolve a git conflict | 100% |
