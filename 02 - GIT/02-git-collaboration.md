# Git
## Git collaboration

### Instructions
In this activity we'll pretend to collaborate in a OSS project, in order to do this fork the repository of your favorite piece of software and perform the following tasks:

- Create a topic branch for a feature that you want added to the software
- Mock up a solution for the feature that you're proposing
- Push the changes back to your fork
- Open up a merge request and add one of your coworkers as a reviewer
- The merge request will be made to the branch you think is more appropriate
- Wait for your coworker to write a review for you, in case it gets approved, merge your topic branch
- Tag your release with the feature you proposed
- Did you squashed your merge? Tell us the reasons behind this decision


### Expected deliverables
- MD file with each activity question answered
- Documentation of procedures, include images if needed



### Evaluation

| Metric name | Description | % Value |
| ----------- |-------------| -------:|
| Branch   | A topic branch was created | 15% |
| Commit   | A meaningful message was left in the commit(s) | 15% |
| Push   | Changes were pushed out of local | 15% |
| Merge   | A merge request (pull request) was made | 15% |
| Tag   | The feature release was tagged | 15% |
| Squash   | Squash inclusion/omission was explained | 25% |
