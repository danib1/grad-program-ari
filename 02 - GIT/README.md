# Git

## Goals
This course will empower the trainee to perform intermediate git tasks and functionality while learning the fundamentals of git internals to allow them to collaborate in software projects.

## Objectives
- Employ the different git commands such as commit, pull, push, checkout, merge, squash, and rebase in a software cycle process
- Explain what a branch is and its usage in the operations/software industry
- Solve git conflicts effectively
- Discriminate the proper scenario to use git stash
- Relate the different areas in the git architecture model (working directory, staging area)

## Contents
- MD as a mean of documentation
- Git setup
- Git initialization
- Git push/pull
- Git merge
- Git stash
- Git conflicts

## Learning materials
- [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Git tutorial ----local-branching-on-the-cheap](https://git-scm.com/docs/gittutorial)
- [PeepCode Git internals](https://github.com/pluralsight/git-internals-pdf/releases/tag/v2.0)
- [Git - the simple guide](https://rogerdudler.github.io/git-guide/)
- [Pro Git book by Scott Chacon and Ben Straub (optional)](https://github.com/progit/progit2/releases/download/2.1.194/progit.pdf)


## Course activities
- [01 - MD format](./01-md-format.md)
- [02 - Git collaboration](./02-git-collaboration.md)
- [03 - About git](./03-about-git.md)
- [04 - Git stash](./04-git-stash.md)
- [05 - Git conflicts](./05-git-conflicts.md)

## Collaborators
Name     |   Slack  | Email |
---------|----------|----------|
Oscar Hernandez    | @Oscar Hernández | oscar.hernandez@digitalonus.com |
Aram Rascon     | @aram | aram.rascon@digitalonus.com |
Rigel Reyes     | @Rigel Reyes | rigel.reyes@digitalonus.com |
