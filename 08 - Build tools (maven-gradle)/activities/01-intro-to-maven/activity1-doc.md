# Maven intro

## Part 1: Hello world

> mvn compile

![](img/mvn-compile-result.png)

Resulting file

![](img/compile-generated-files.png)

> mvn package

![](img/mvn-package-result.png)

Resulting files

![](img/package-generated-files.png)

Executing the Jar:

![](img/executing-the-jar.png)


## screenshot of the resulting file structure

[My Forked Spring Guide Repo](https://github.com/danibyay/gs-maven)
