# TERRAFORM 

###### Course duration – 3 days

## Goals of the course

Trainees will be led to make complete infrastructure as code on different providers (AWS & Kubernetes). From a series of Terraform configurations, they will learn to manage the entire infrastructure life cycle, to create, modify and eliminate it. Most common architectures and best practices.  

## Learning objectives 

- Recognize immutable infrastructure. 
- Identify when to use Terraform and/or configuration management tools. 
- Apply DevOps techniques with Terraform. 
- Integrate Terraform to CICD tools. 

 

## Contents 

- The basics of how Terraform works 
	- HCL 
	- Providers 
- Infrastructure life cycle. 
	- Init 
	- Plan 
	- Apply 
	- Destroy 
- How to install the Terraform 0.11 and 12 
	- Differences 0.11 vs 0.12 
- Workspaces and Environment management. 
	- Workspaces to manage environments. 
	- Architectures to manage environments without workspaces. 
- Create Terraform modules 
	- Local modules 
	- Remote modules 
		- Creating a repository to store modules. 
		- Using remote module URL in environments. 
	- Usage of outputs to pass data to other modules. 
- How to use Terraform to deploy infrastructure such as AWS and Kubernetes. 
	- Use AWS provider. 
	- Use Kubernetes provider.  
- Managing Terraform state 
	- Backend types. 
	- How to setup a backend. 
- Using Terraform in a CI/CD environment 
	- How to manage infrastructure life cycle through Jenkins. 
	- Seeing other options for CICD with gitlab-ci Seeing other options for CICD with gitlab-ci* 


## Learning materials 

- [terraform_slides](https://digitalonus01.sharepoint.com/:p:/r/_layouts/15/Doc.aspx?sourcedoc=%7B0ECC5406-4E1C-47C0-8E2C-CE120AB944A7%7D&file=DOU%20University%20-%20Terraform.pptx&action=edit&mobileredirect=true)
- Terraform: Up and Running 
ISBN: 978-1-491-97708-8 
- Terraform Docs 
https://www.terraform.io/docs/index.html 
- Terraform Recommended Practices 
https://www.terraform.io/docs/cloud/guides/recommended-practices/index.html
- Terraform Backends
https://www.terraform.io/docs/backends/index.html

## Course activities 

- 01 – My first IaC project using Terraform 
- 02 -  Working on teams and managing backends
- 03 - Integrating Terraform to CICD tools


##### Collaborators 

| Collaborator  | Slack  |
| ------------ | ------------ |
| Monserrat Sedeno   |@Monse   |
| Jesus Gomez  |@JesusG   |
 
