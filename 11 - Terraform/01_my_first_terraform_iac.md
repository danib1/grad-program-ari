## 01 – My first IaC project using Terraform 



##### Instructions

Trainees will be given the tasks to build basic infrastructure in their preferred provider.
1. Create repository for the project
2. Choose a provider and design a simple infrastructure
3. Create your file structure
4. Add the resources, proviers and all objects that will compose the project
5. Validate, Plan, Apply, Destroy
5. ??
6. Profit


##### Expected deliverables 

Terraform Infrastructure code for either docker, kubernetes or aws providers.


##### Measuring instrument 

| Metric  |  Description | Value  |
| ------------ | ------------ | ------------ |
|  Architectural adherence | Files organization and objects  | 40%  |
|   Functional| Clear idea and working  |  40% |
|  Integration with other tools | In this case, the proper setup of the platform to work with   |  20% |


