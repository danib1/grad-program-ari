# Intro to Devops

| # | Function of DevOps |Tasks performed | Tools | what are the tools used for. |
| - |----------- |------------ |-------------- |-------------- |
| 1 | Collaboration| Selective alerts.| ServiceNow, Jira, Trello, Slack, Stride, Remedy, OpsGenie, PagerDuty, Agile Central| Project managament, issue tracking, |
| 2 |Continuous Integration | Build and notify | Jenkins, Bamboo, Codeship, VSTS, Travis, Circle, Teamcity, AWS CodeBuild| Builds of any programming language.  |
| 3 | Configuration| Configure multi-node software| Chef, Ansible, Rudder, Packer, Puppet, Salt, CFEngine | Streamlines configuration and maintenance of servers|
| 4 | Containers| deployment | Docker, Kubernetes, Mesos, Rancher, GKE, AKS, RKT, Helm, Codefresh| Resource isolation, virtualization, automation.|
| 5 | Orchestration| manage | Kubernetes, Nomad, Amazon Elastic Container Service, Volcano, Docker Swarm| Allows you to run and scale containerized applications.|
| 6 |Monitoring | notify | Nagios, Zabbix, Zenoss, Bugzilla, Trello, Jira, Mantis Bug Tracker.| Ensure services and applications are always on.|
| 7 | Security| Secure, store and control access | Snort, Tripwire, CyberArk, Veracode, FortifySCA, Signal Sciences, BlackDuck, SonarQube, Vault| Intrusion prevention|


## Tools sorted by open source and not open source

| open source / free /freemium | paid / enterprise|
| - | - |
| Gitlab, Github, Subversion, Artifactory, Nexus, Bitbucket, Flyway, Jenkins, Travis, Circle, Codeship, VSTS, Teamcity, | Perforce, Datical, DBMaestro, Delphix, Redgate, Bamboo, AWS codebuild |
