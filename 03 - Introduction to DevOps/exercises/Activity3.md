# Learning DevOps Concepts

## What does DevOps mean?

One way to explain DevOps is operations working together with engineers to get things done faster in an automated and repeatable way.

It is a cultural and professional movement, focused on how we build and operate high velocity organizations, born from the experiences of its practitioners.

Devops is a philosophy and a set of practices where technology teams will integrate to have better communication and be more efficient.

## How is DevOps different from Agile?

Agile is a concept that existed before DevOps.

| DevOps | Agile |
| - | - |
| Devops extends to a wider scope | Agile covers the development problem |
| Focuses on constant testing and delivery | Focuses on constant changes of anything |
| May require a large team | Requires a small team |
| Operational and business readiness | Functional and non-functional readiness |
|Plan, code, test, release, deploy, operate, automate | Plan, code test|

Both focus on quality and on an agile cycle. Wether it's for development or release.

## What is the origin of DevOps?

In 2009 an experience based movement was started by a group of practitioners.

Devops responds to the needs of current software practices, the rate of new deliveries has accelerated. Software needs to be continously updated, and Devops offers that fast feedback and update systems.

## How can DevOps improve team relationships?

Devops will not eliminate the role of development, or operations, those roles continue to exist, they just work better together.

Devops needs a culture of sharing and collaboration in order to be able to measure the efficiency of the processes that involve development operation. It is a virtuous cycle. To put everything into practice, the team needs to change, and good results will encourage to continue with the good practices.


![](cycle.png)

## Explain Continuous Integration.

Continuous integration has been around for a while.

It refers to integrating individual code with the overall development environment after building and testing it.

CI Tools ensure that the code is compiled, run, and tested before integrating with the rest

Continuous integration ensures code worked by several developers across multiple locations are always integrated into a common repository. This avoids the chaos that results from bulky and conflicting code commits.

## Explain Continuous Delivery.

Continuous delivery is an ongoing DevOps practice of building, testing, and delivering improvements to software code and user environments with the help of automated tools. The key outcome of the continuous delivery (CD) paradigm is code that is always in a deployable state.

Continuous delivery is a layer on top of continuous integration.

## Explain Continuous Testing.

Continuous Testing involves a process of testing early, testing often, test everywhere, and automate.

It evaluates quality at every step of the Continuous Delivery Process.

## Explain Continuous Monitoring.

Monitoring must be automated in the same way integration, testing, and deployment have become automated.

In highly dynamic and scaled environments, the process of monitoring microservices must adapt to changes without manual intervention and configuration.

Monitoring automation must be applied to infrastructure, application, and orchestration technologies.

This way you can be automatically alerted of infrastructure and performance problems, automatically visualize components and dependencies of the application, etc.

## What is the difference between Continuous Delivery and Continuous Deployment?

Continuous deployment takes the model one step further by automatically deploying code to production after each codecommit and build.

While in continuous delivery, the deployment follows an on-demand model; here it is pushed automatically every time. This can only work in highly mature DevOps teams.

| Cont delivery | Cont deployment|
|-|-|
| Automates from committed code, to a deploy ready state | Automates from committed code to deployed code. |

## What is configuration management in terms of infrastructure and mention a few popular tools used?

Configuration management solves the problem of having to manually install and configure packages once the hardware is in place.

The benefit of using configuration automation solutions is that servers are deployed exactly the same way every time. If you need to make a change across ten thousand servers you only need to make the change in one place.

Some tools are:

- Chef
- Puppet
- Ansible
- Salt Stack
- Pallet
- Bcfg2

## Which scripting tools are used in DevOps?

- Bash
- Python
- Ruby
- Javascript
- Go

### References

[Intro to Devops.](https://devops.com/introductiontodevops/) from Devops world.

[Continuous Delivery](https://www.sumologic.com/insight/continuous-delivery/) from sumo logic.

[Continuous deployment and monitoring](https://dzone.com/articles/continuous-deployment-and-continuous-monitoring-a) from Devops zone.

[Continuous testing](https://www.guru99.com/continuous-testing.html) from Guru 99.

[Top 5 languages for Devops](https://opensource.com/article/17/4/top-5-programming-languages-devops) from Opensource.
