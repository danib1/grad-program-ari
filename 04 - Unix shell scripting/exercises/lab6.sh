#!/bin/bash

# instructions:
# use this script with 2 additional arguments that must be text files


file1=$1
file2=$2

no_files_code=65
not_two_files=85
files_dont_exist=87
empty_files=89

# Check for the number of arguments
# if no arguments were passed
if [ $# -eq 0 ]
then
  echo "Error: No files given"
  exit $no_files_code
# if the arguments are not exactly two
elif [ $# -eq 1 ] || [ $# -gt 2 ]
then
  echo "Error: The script lab6.sh must contain exactly 2 files, for example: file1.txt file2.txt"
  exit $not_two_files
# if there are two arguments passed, validate further.
elif [ $# -eq 2 ]
then
  # check if both files exist
  if test -f "$file1" && test -f "$file2"
  then
    # find out the size of the files
    file1size=$(wc -c $file1 | awk '{print $1}')
    file2size=$(wc -c $file2 | awk '{print $1}')
    # validate size is greater than 0
    if [ "$file1size" -gt 0 ] && [ "$file2size" -gt 0 ]
    then
      touch lab6_output.txt
      # merge contents alphabetically into new file
      cat $file1 $file2 | sort -k 2 > lab6_output.txt
      echo "Execution of the script lab6.sh ran successfully"
    else
      echo "Error: files are 0 size"
      exit $empty_files
    fi
  else
    echo -e "Error: One or more of the files $file1 and $file2 dont exist"
    exit $files_dont_exist
  fi
fi
