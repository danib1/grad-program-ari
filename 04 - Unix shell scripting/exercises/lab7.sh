#!/bin/bash

# instructions:
# Pass to this script an existing user of this OS as an argument

# this script will print
# the groups that the user belongs to in separate lines

# use if and while and functions

user=$1

# validate if the user exists


how_many_groups=$(groups $user | wc -w $file1)

print_groups () {
  echo -e "the user belongs to $how_many_groups groups"
  for i in $(id -Gn $user);do echo "  - $i" ;done
}


print_groups
