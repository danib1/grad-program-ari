#!/bin/bash

# This file will create as many files as the user wants
# with a limit of 20 for practical reasons.

echo "hello, we will create files with the mylabfile prefix for you"
echo "how many files do you want ? (between 1 and 20)"

read number_of_files

for (( i=1; i<=$number_of_files; i++ ))
do
  touch mylabfile$i.txt
done
