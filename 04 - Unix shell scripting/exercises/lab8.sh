#!/bin/bash

# Running this script needs epel and git installed.

sudo yum -y install nginx
sudo yum -y erase nginx

sudo systemctl enable nginx
sudo systemctl start nginx
