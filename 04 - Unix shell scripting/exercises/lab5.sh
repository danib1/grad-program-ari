#!/bin/bash

# Calculator app

choice=0
while [ "$choice" -ne 5 ]
do
  echo -e "Choose the operation you want to make (only from 1 to 5) \n"
  echo "1. Addition"
  echo "2. Substraction"
  echo "3. Multiplication"
  echo "4. Division"
  echo "5. Exit"

  read choice
  sleep 1
  clear

  if [ "$choice" -ne 5 ]
  then
    echo "Enter the first number"
    read first
    echo "Enter the second number"
    read second
  fi

  if [ "$choice" -eq 1 ]
  then
    echo -e "$first plus $second is $(($first+$second))"
  elif [ "$choice" -eq 2 ]
  then
    echo -e "$first minus $second is $(($first-$second))"
  elif [ "$choice" -eq 3 ]
  then
    echo -e "$first times $second is $(($first*$second))"
  elif [ "$choice" -eq 4 ]
  then
    echo -e "$first divided by $second is $(($first/$second))"
  elif [ "$choice" -eq 5 ]
  then
    echo "Goodbye!"
  else
    echo "Invalid option, please try again"
  fi

  sleep 1
  clear

done
