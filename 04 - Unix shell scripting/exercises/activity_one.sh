#!/bin/bash
# create a script that, when run, will accept two command line values as arguments
# These arguments should be a username and password and assigned to two variables
# in the script named appropriately. Finally, echo those values out to the
# terminal when run to indicate they were read and assigned as expected.

s_user=$1
s_password=$2

echo -e "the user is $s_user and the password is $s_password"
