#!/bin/bash

# Create a simple text file at the command prompt.
# This file should contain three values - CPU, Memory and Disk
# space for an imaginary system, all on one line and delimited
# with a '|' character.

# Write a script to read that file and
# prompt the user for the delimiter value.
# Use that delimiter along with the IFS variable and read those delimited
# values into three appropriately named variables. Finally, display
# them with labels, one each per line.

echo "enter a separator character:"
read separator
IFS=$separator

file=my_system.txt
eval 'rfile=($(cat /Users/DaniB/Documents/DOU/grad-program/my_system.txt))'
echo -e "Reading from $file \n"

cpu="${rfile[0]}"
memory="${rfile[1]}"
disk="${rfile[2]}"

echo -e "Number of cpu cores is $cpu"
echo -e "GB of memory are $memory"
echo -e "TB of disk are $disk"
