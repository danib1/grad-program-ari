#!/bin/bash
# Create a script that, when run, will display the
# following environment variables to the console
# PATH ENV SHELL

echo -e "your path is $PATH\n"
echo -e "your env is $ENV\n"
echo -e "your shell is $SHELL\n"
