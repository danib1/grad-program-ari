#!/bin/bash

# This is a very simple bash script
# To run it on debug mode execute
# bash -x lab1.sh


hello_world () {
  echo "Enter a name for the file"
  read filename
  echo -e "hello world" > $filename.txt
}

hello_world
