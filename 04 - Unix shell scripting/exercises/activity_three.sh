#!/bin/bash
# Evaluate an arithmetic expression
# Attempt to remove a file that does not exist in the current directory
# Evaluate another arithmetic expression
# Immediately after each command, echo the exit status of that
# command to the terminal using the appropriate variable to show
# success and failure exit codes.

echo "let's find out how much is 5 times 23942: $((5*23942))"
echo -e "exit status is: $? \n"
rm whatever.txt
echo -e "exit status is: $? \n"
echo "let's find out how much is 5020423 divided by 4: $((5020423/4))"
echo -e "exit status is: $?"
