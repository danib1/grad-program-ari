#!/bin/bash

# This script will output 10 random numbers when executed

count=0
while [ "$count" -lt 10 ]
do
  number=$((RANDOM))
  echo -e "$number"
  count=$((count+1))
done
