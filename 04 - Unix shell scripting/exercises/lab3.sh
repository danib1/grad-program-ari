#!/bin/bash

# The arrow keys produce more than one character
# we need to save the three parts separately and then
# group them to parse them.

echo "Please press any arrow key"
read -sn1  k1
read -sn1  k2
read -sn1  k3

key+=${k1}${k2}${k3}

case $key in
  $'\e[A'|$'\e0A')  # up arrow
        ((cur > 1)) && ((cur--))
        echo you pressed up;;

  $'\e[D'|$'\e0D') # left arrow
        ((cur > 1)) && ((cur--))
        echo you pressed left;;

  $'\e[B'|$'\e0B')  # down arrow
        ((cur < $#-1)) && ((cur++))
        echo you pressed down;;

  $'\e[C'|$'\e0C')  # right arrow
        ((cur < $#-1)) && ((cur++))
        echo you pressed right;;

  *) echo you pressed something else;;

esac
