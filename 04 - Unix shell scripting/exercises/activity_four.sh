#!/bin/bash
# Write a script intended to iterate through an array called
# SERVERLIST containing at least four values (server names).
# Display all four values to the terminal when run.


declare -a SERVERLIST=("prod1" "testing2" "usa1" "pacific1" "gmt_server" "prod2")

for serv in ${SERVERLIST[@]}
do
echo $serv
done
