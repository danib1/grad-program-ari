# Unix Shell Scripting
### Checking Users in your O.S.
------------
##### Instructions
1.  Create the script file with the proper name
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Declare variables
5.  Work with conditionals if
6.  Use while
7.  Work with parameters
8.  Use functions
##### Expected deliverables
Pass the script a required parameter, this parameter can be any user in the OS as long as it exists. The script should output all of the corresponding groups that the user belongs to, print each group in an individual line.
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is short, understandable, clean, light etc  |  20 |
|  Instructions | Instructions are included within the script  |  40 |
| Deliverable  | Script is working properly  |  40 |
