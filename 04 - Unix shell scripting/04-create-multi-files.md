# Unix Shell Scripting
### Create multi files 
------------
##### Instructions
1.  Create the script file with the proper name
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Use read
5.  Work with the loop for
##### Expected deliverables
Ask to the user how many files want to create, take the amount  and create that amount of text files automatically.
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is easy readable |  20 |
|  Instructions | Comments instructions are included within the script  |  40 |
| Deliverable  | Script is working properly  |  40 |
