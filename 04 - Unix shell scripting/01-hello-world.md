# Unix Shell Scripting
### Hello World
------------
##### Instructions
1.  Create the script file
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Run the script in debug mode
5.  Use functions
6.  Give to the script some format like newlines and tab
7.  Use read 
8.  Redirect the text (Hello World) to a new text file
##### Expected deliverables
Ask to the user input a file name, a text file must be created with the same name that the user inputted and within the file must contain the text "Hello World"
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is readable & clean  |  20 |
|  Instructions | Comments with instructions are included within the script  |  40 |
| Deliverable  | Script is working properly  |  40 |
