# Unix Shell Scripting
### Arithmetic Operations
------------
##### Instructions
1.  Create the script file with the proper name
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Use read
5.  Work with Arithmetic Operations
##### Expected deliverables
Display a menu to perform basic arithmetic operations as following:
Which operation would you like to do:
1. Addition
2. Substraction
3. Multiplication
4. Division
5. Exit
When an option is chosen, ask to the user for 2 different numbers to perform the arithmetic operation appropriate, show the result in the shell  and return to the menu automatically to choose another option until the user press 5 (exit) NOTE: If the user press any other character(number or letter) out of the menu, show a prompt "Just choose from 1 to 5 please ..."
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is easy readable  |  20 |
|  Instructions | Comments intrusctions are included | 40 |
| Deliverable  | Script is working properly  |  40 |
