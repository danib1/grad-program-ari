# Unix Shell Scripting
### Random numbers
------------
##### Instructions
1.  Create the script file with the proper name
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Declare variables
5.  Work with the loop while
6.  Set up the script to show 10 random numbers
##### Expected deliverables
Run the script and it shows me 10 random numbers in the shell
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is readable & clean  |  20 |
|  Instructions | Comments instructions are included within the script  |  40 |
| Deliverable  | Script is working properly  |  40 |
