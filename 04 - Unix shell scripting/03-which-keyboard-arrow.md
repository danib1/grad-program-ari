# Unix Shell Scripting
### Which Keyboard Arrow
------------
##### Instructions
1.  Create the script file with the proper name
2.  Make sure your script is built with the Shebang interpreter
3.  Write a comment within the script
4.  Declare variables
5.  Use read
6.  Work with case
##### Expected deliverables
Script must ask to the user to press any keyboard arrow (up, down, left, right), and the script must show in the shell which keyboard arrow the user pressed
##### Evaluation
| Metric Name  | Description  | % Vaue  |
| ------------ | ------------ | ------------ |
|  Quality | Script is readable  |  20 |
|  Instructions | Comments instructions are included within the script  |  40 |
| Deliverable  | Script is working properly  |  40 |
