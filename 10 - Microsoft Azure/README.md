# Microsoft Azure

## Goals
This course will provide a basic introduction to Azure concepts and also will drive through the most common Azure services.

## Objectives
- Explain the basic concepts of Azure and cloud computing
- Describe the differences between Infrastructure-as-a-Service (IaaS), Platform-as-a-Service (PaaS) and Software-as-a-Service (SaaS)
- Classify the core Azure architectural components
- 

## Contents
- Cloud Concepts
    - Regions 
    - Availability Zones 
    - High Availability 
    - Fault Tolerance 
    - Disaster Recovery 
    - Scalability 
    - Elasticity 
- Azure Services 
    - IaaS, PaaS, SaaS 
    - Computing 
    - Network 
    - Storage 
- Security 
- Azure Pricing 


## Learning materials
 Course                           | URL
---------------------------------| -----------------------------------------------
High Availability, Fault Tolerance, and Disaster Recovery |https://linuxacademy.com/cp/courses/lesson/course/4073/lesson/2
Azure Regions  | https://azure.microsoft.com/en-us/global-infrastructure/regions
Azure Infrastructure | https://azure.microsoft.com/en-us/global-infrastructure/
Scalability and Elasticity | https://linuxacademy.com/cp/courses/lesson/course/4073/lesson/3/module/330

## Course activities
- [01 - Cloud Basic Concepts](./01-cloud-basic-concepts.md)
- [02 - Creating a Virtual Machine](./02-creating-vm.md)
- [03 - Creating Azure Virtual Networks](./03-network-creation.md)


## Collaborators
Name     |   Slack  | Email |
---------|----------|----------|
Monserrat Sedeno    | @Monse | monserrat.sedeno@digitalonus.com |
Juan Francisco Gil | @Francisco Gil | francisco.gil@digitalonus.com |
