# Microsoft Azure
# 01 - Cloud Basic Concepts

### Instructions
Take the following Azure courses:
    - [High Availability, Fault Tolerance, and Disaster Recovery](https://linuxacademy.com/cp/courses/lesson/course/4073/lesson/2)
    - [Scalability and Elasticity](https://linuxacademy.com/cp/courses/lesson/course/4073/lesson/3/module/330)
Describe *in your own words* the following concepts:
*  Region, High Availability
*  Fault Tolerance
*  Disaster Recovery
*  Scalability 
*  Elasticity

### Expected deliverables
- MD file with each activity
- Documentation of procedures, include images if needed

### Evaluation

| Metric name        | Metric description                                | % Value |
|:------------------ |:--------------------------------------------------|:--:|
| MD format  | Clearity and completeness of their research | 40% |
| Content |  Was able to explain in his/her own words the concepts | 60% |
